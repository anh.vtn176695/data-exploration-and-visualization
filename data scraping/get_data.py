
import csv
from requests_html import HTMLSession

with open('car_data_sale.csv', mode='w', newline='', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['car_model', 'status', 'km', 'gear_box', 'fuel', 'seller', 'car_type',
                     'car_year', 'driver_system', 'color', 'origin', 'price', 'area'])

    for i in range(1, 28):
        session = HTMLSession()
        url = "https://choxe.net/xe-moi?page=" + str(i)
        r = session.get(url)
        rs = r.html.find(".list-news .info-left .img ", first=False)
        for i in rs:
            hang_url = i.attrs['href']
            r1 = session.get(hang_url)
            rs1 = r1.html.find(".hidden-xs .detail-xe b", first=False)
            price = r1.html.find("#col-fix > div > div.box-gia-ban.hidden-xs > span", first=True).text
            area = r1.html.find("#col-fix > div > div.bg-white > div.short-info > p > span", first=True).text
            car_model = r1.html.find("body > div.main-content.bg-light-blue > div.container.relative > div.row.m0.des-xe > h1", first=True).text
            writer.writerow([car_model, rs1[0].text, rs1[1].text, rs1[2].text, rs1[3].text, rs1[4].text, rs1[5].text, rs1[6].text, rs1[7].text, rs1[8].text, rs1[9].text, price, area])
            print('Writing...')





